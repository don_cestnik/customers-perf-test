# Customers Performance Test
Simple performance test for customers web service written using the http://gatling.io/ tool.
Written for a some basic testing locally. Probably not reusable or valuable enough to be sent to sofi repo.

## Running
In order to run execute the following:
```bash
sbt gatling:test
```

After test is complete you can review the results at `file:///<PATH_TO_PROJECT>/customers-perf-test/target/gatling/` using web browser.

## Customizing
Edit the file src/test/scala/customers/CustomersTest.scala most likely editing these values:
``` scala
val endpointUrl = "http://localhost:9024/cs/v1/customers"
val numUsers = 10
val rampUpTime = 10 seconds
```
