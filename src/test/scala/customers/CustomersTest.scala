package customers

import java.text.SimpleDateFormat
import java.util.Calendar

import scala.concurrent.duration.DurationInt

import io.gatling.core.Predef.Simulation
import io.gatling.core.Predef.StringBody
import io.gatling.core.Predef.array2FeederBuilder
import io.gatling.core.Predef.checkBuilder2Check
import io.gatling.core.Predef.configuration
import io.gatling.core.Predef.defaultJsonParsers
import io.gatling.core.Predef.defaultJsonPathExtractorFactory
import io.gatling.core.Predef.exec
import io.gatling.core.Predef.findCheckBuilder2CheckBuilder
import io.gatling.core.Predef.findCheckBuilder2ValidatorCheckBuilder
import io.gatling.core.Predef.intToFiniteDuration
import io.gatling.core.Predef.rampUsers
import io.gatling.core.Predef.scenario
import io.gatling.core.Predef.stringToExpression
import io.gatling.core.Predef.value2Expression
import io.gatling.http.Predef.http
import io.gatling.http.Predef.jsonPath
import io.gatling.http.protocol.HttpProtocolBuilder.toHttpProtocol
import io.gatling.http.request.builder.HttpRequestBuilder.toActionBuilder

class CustomersTest extends Simulation {
    // values likely to change per test
    val endpointUrl = "http://localhost:9024/cs/v1/customers"
    val numUsers = 10000
    val rampUpTime = 15 minutes
    val dateStr = new SimpleDateFormat("yyyyMMdd_kkmmss").format(Calendar.getInstance.getTime)

    val feederValues = 1.to(numUsers)
            .map(x => "dcestnik+%s_%s@sofi.org".format(dateStr, x))
            .map(x => Map("email" -> x))
            .to[Array]
    val postRequest = http("PostCustomer").post("").body(StringBody("""
            {
              "email":"${email}",
              "firstName":"don",
              "lastName":"cestnik",
              "status":"REGI",
              "noCollege":false,
              "noCollegeGraduation":false,
              "profileRecent":false,
              "addresses":[ ],
              "customerRaces":[ ],
              "identities":[ ],
              "identifiers":[ ],
              "employmentHistory":[ ],
              "contactPreferences":[ ],
              "consents":[ ],
              "educationHistory":[ ],
              "formattedName":"don cestnik",
              "identity":{ "present":false }
            }"""))
    val getRequest = http("GetCustomer").get("/${customerId}")
    val putRequest = http("PutCustomer").put("/${customerId}").body(StringBody("""
            {
              "id":"${customerId}",
              "email":"${email}",
              "firstName":"notdon",
              "lastName":"cestnik",
              "status":"REGI",
              "noCollege":false,
              "noCollegeGraduation":false,
              "profileRecent":false,
              "addresses":[ ],
              "customerRaces":[ ],
              "identities":[ ],
              "identifiers":[ ],
              "employmentHistory":[ ],
              "contactPreferences":[ ],
              "consents":[ ],
              "educationHistory":[ ],
              "formattedName":"don cestnik",
              "identity":{ "present":false },
              "modifiedBy":"dcestnik@sofi.org"
            }"""))

    val postCustomerScenario = scenario("CustomersTest")
            .feed(feederValues)
            .exec(postRequest.check(jsonPath("$.id").saveAs("customerId")))
            .pause(5 seconds)
            .repeat(10) { exec(getRequest.check(jsonPath("$.firstName").is("don"))) }
            .pause(5 seconds)
            .exec(putRequest)
            .pause(5 seconds)
            .repeat(10) { exec(getRequest.check(jsonPath("$.firstName").is("notdon"))) }
            .inject(rampUsers(numUsers) over rampUpTime)
    setUp(postCustomerScenario).protocols(http.baseURL(endpointUrl).header("Content-Type", "application/json"))
}
